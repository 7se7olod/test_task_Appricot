import Foundation

// MARK: - Welcome
struct Welcome: Codable {
  let results: [Result]
}

// MARK: - Result
struct Result: Codable {
  let id: Int
  let name: String
  let status: Status
  let species: Species
  let type: String
  let gender: Gender
  let origin, location: Location
  let image: String
  let episode: [String]
  let url: String
  let created: String
}

enum Gender: String, Codable {
  case female = "Female"
  case male = "Male"
  case unknown = "unknown"
}

// MARK: - Location
struct Location: Codable {
  let name: String
  let url: String
}

// MARK: - Species
enum Species: String, Codable {
  case alien = "Alien"
  case human = "Human"
}

// MARK: - Status
enum Status: String, Codable {
  case alive = "Alive"
  case dead = "Dead"
  case unknown = "unknown"
}
