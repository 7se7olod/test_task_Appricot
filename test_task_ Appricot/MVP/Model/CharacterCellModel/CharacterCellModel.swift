import Foundation

struct CharacterCellModel {
  let name: String
  let species: String
  let gender: String
  let image: String
  let status: String
  let location: String
  let episodes: String
}
