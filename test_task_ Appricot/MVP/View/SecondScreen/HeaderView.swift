import UIKit

class HeaderView: UIView {

  private let networking = Network()

  // MARK: - labels
  let nameLabel: UILabel = {
    let label = UILabel()
    return label
  }()

  let speciesLabel: UILabel = {
    let label = UILabel()
    return label
  }()

  let genderLabel: UILabel = {
    let label = UILabel()
    return label
  }()

  // MARK: - image view
  let characterImage: UIImageView = {
    let imageView = UIImageView()
    imageView.layer.cornerRadius = 15
    imageView.layer.borderWidth = 2
    imageView.clipsToBounds = true
    return imageView
  }()

  // MARK: - configuration cell
  private func configHeader() {
    self.addSubview(characterImage)
    characterImage.snp.makeConstraints { make in
      make.left.equalToSuperview().inset(20)
      make.top.bottom.equalToSuperview().inset(10)
      make.width.height.equalTo(100)
    }

    self.addSubview(nameLabel)
    nameLabel.snp.makeConstraints { make in
      make.left.equalTo(characterImage.snp.right).offset(20)
      make.top.right.equalToSuperview().inset(10)
    }

    self.addSubview(speciesLabel)
    speciesLabel.snp.makeConstraints { make in
      make.left.equalTo(characterImage.snp.right).offset(20)
      make.top.equalTo(nameLabel.snp.bottom).offset(10)
      make.right.equalToSuperview().inset(10)
    }

    self.addSubview(genderLabel)
    genderLabel.snp.makeConstraints { make in
      make.left.equalTo(characterImage.snp.right).offset(20)
      make.top.equalTo(speciesLabel.snp.bottom).offset(10)
      make.right.equalToSuperview().inset(10)
    }
  }

  // MARK: - set image
  func setCharacterImage(urlString: String) {
    self.networking.getImage(urlString: urlString) { dataImage in
      self.characterImage.image = UIImage(data: dataImage)
    }
  }

  // MARK: - set labels
  func setCharacterLabel(name: String, gender: String, species: String) {
    self.nameLabel.text = name
    self.genderLabel.text = gender
    self.speciesLabel.text = species

    self.configHeader()
  }

}
