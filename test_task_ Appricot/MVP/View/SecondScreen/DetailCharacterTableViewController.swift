import UIKit

class DetailCharacterTableViewController: UITableViewController, DetailCharacterViewable {

  // MARK: - presenter
  var presenter: DetailCharacterPresentProtocol?

  // MARK: - life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = TitleController.detail.rawValue
    self.configTableView()
  }

  // MARK: - config table view
  private func configTableView() {
    self.tableView.register(DetailCell.self, forCellReuseIdentifier: DetailCell.reuseID)
  }

  // MARK: - show character
  func showCharacter() {
    self.tableView.reloadData()
  }


  // MARK: - number of sections
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  // MARK: - number of rows sections
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter?.extraInfoCharacter.count ?? 0
  }

  // MARK: - view for header in section
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = HeaderView()
    guard let character = presenter?.detailCharacter else { return nil }
    headerView.setCharacterLabel(name: character.name,
                                 gender: character.gender,
                                 species: character.species)
    headerView.setCharacterImage(urlString: character.image)
    if character.status == StatusCharacter.dead.rawValue {
      headerView.characterImage.layer.borderColor = UIColor.red.cgColor
    } else if character.status == StatusCharacter.alive.rawValue {
      headerView.characterImage.layer.borderColor = UIColor.systemGreen.cgColor
    }

    return headerView
  }

  // MARK: - cell for row at index path
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard
      let cell = tableView.dequeueReusableCell(withIdentifier: DetailCell.reuseID, for: indexPath) as? DetailCell else {
        return UITableViewCell()
      }

    guard let character = presenter?.extraInfoCharacter else { return UITableViewCell() }
    cell.textLabel?.text = character[indexPath.row]

    return cell
  }
}
