import Foundation

protocol DetailCharacterViewable: AnyObject {
  func showCharacter()
}
