import Foundation

protocol ListCharacterViewable: AnyObject {
  func showCharacters()
}
