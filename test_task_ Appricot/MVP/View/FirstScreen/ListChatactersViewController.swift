import UIKit

class ListChatactersViewController: UITableViewController, ListCharacterViewable {

  // MARK: - presenter
  private var presenter: CharacterListPresenterProtocol?

  // MARK: - life cycle
  override func loadView() {
    super.loadView()
    self.presenter = CharacterListPresenter(delegate: self)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = TitleController.all.rawValue
    self.configTableView()
  }

  // MARK: - config table view
  private func configTableView() {
    self.tableView.register(CharacterCell.self, forCellReuseIdentifier: CharacterCell.reuseID)
  }

  // MARK: - List character viewable
  func showCharacters() {
    self.tableView.reloadData()
  }


  // MARK: -number of sections
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  // MARK: - number of rows in section
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter?.characters.count ?? 0
  }

  // MARK: - height for row at index path
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }

  // MARK: - cell for row at index path
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: CharacterCell.reuseID, for: indexPath) as? CharacterCell else { return UITableViewCell() }

    guard let character = presenter?.characters[indexPath.row] else { return UITableViewCell() }
    cell.setCharacterLabel(name: character.name, gender: character.gender, species: character.species)
    cell.setCharacterImage(urlString: character.image)

    return cell
  }

  // MARK: - did select row at index path
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let character = presenter?.characters[indexPath.row] else { return }
    let detailTableViewController = DetailCharacterTableViewController()
    detailTableViewController.presenter = DetailCharacterPresenter(delegate: detailTableViewController, character: character)
    navigationController?.pushViewController(detailTableViewController, animated: true)    
  }
}
