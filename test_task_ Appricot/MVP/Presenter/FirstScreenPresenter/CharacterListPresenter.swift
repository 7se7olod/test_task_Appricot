import Foundation

class CharacterListPresenter: CharacterListPresenterProtocol {

  // MARK: - delegate
  weak var delegate: ListCharacterViewable?

  // MARK: - properties
  private let networking = Network()
  var characters: [CharacterCellModel] = []


  // MARK: - initialization
  init(delegate: ListCharacterViewable) {
    self.delegate = delegate
    self.getCharacterListData()
  }

  // MARK: - get characters
  func getCharacterListData() {
    self.networking.getCharacterList(requestType: .characterListURL) { response in
      let results = response.results
      DispatchQueue.main.async {
        results.forEach { character in
          let character = CharacterCellModel(name: character.name,
                                             species: character.species.rawValue,
                                             gender: character.gender.rawValue,
                                             image: character.image,
                                             status: character.status.rawValue,
                                             location: character.location.name,
                                             episodes: String(character.episode.count))
          self.characters.append(character)
        }
        self.delegate?.showCharacters()
      }
    }
  }
}
