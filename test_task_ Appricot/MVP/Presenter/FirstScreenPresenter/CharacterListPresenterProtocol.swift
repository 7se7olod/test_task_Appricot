import Foundation

protocol CharacterListPresenterProtocol {

  var characters: [CharacterCellModel] { get set }

  func getCharacterListData()
}
