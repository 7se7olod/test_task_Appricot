import Foundation

protocol DetailCharacterPresentProtocol {

  var detailCharacter: CharacterCellModel? { get set }

  var extraInfoCharacter: [String] { get set }

}
