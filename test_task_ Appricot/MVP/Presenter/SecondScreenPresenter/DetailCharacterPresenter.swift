import Foundation

class DetailCharacterPresenter: DetailCharacterPresentProtocol {

  // MARK: - delegate
  weak var delegate: DetailCharacterViewable?

  // MARK: - properties
  var detailCharacter: CharacterCellModel?
  var extraInfoCharacter: [String] = []

  // MARK: - initialization
  init(delegate: DetailCharacterViewable, character: CharacterCellModel?) {
    self.delegate = delegate
    self.detailCharacter = character
    self.showExtraInfo()
  }

// MARK: - method
  func showExtraInfo() {
    guard let char = detailCharacter else { return }
    let status = char.status
    let location = char.location
    let episodes = char.episodes

    extraInfoCharacter.append("Status: \(status)")
    extraInfoCharacter.append("Location: \(location)")
    extraInfoCharacter.append("Episodes: \(episodes)")
    delegate?.showCharacter()
  }
}
