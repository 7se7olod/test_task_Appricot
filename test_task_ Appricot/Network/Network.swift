import UIKit
import Alamofire

class Network {

    // MARK: - request types
    enum RequestType: String {
      case characterListURL = "https://rickandmortyapi.com/api/character"
    }

    // MARK: - get data
    func getCharacterList(requestType: RequestType, completion: @escaping (Welcome) -> Void) {

      AF.request(requestType.rawValue).response { response in
        guard let data = response.data else { return }
        do {
          let decoder = JSONDecoder()
          let json = try decoder.decode(Welcome.self, from: data)
          DispatchQueue.main.async {
            completion(json)
          }
        }
        catch {
          print("\(error)")
        }
      }
    }

  // MARK: - get image
    func getImage(urlString: String, completion: @escaping (Data) -> Void) {
      AF.request(urlString).response { response in
        guard let dataImage = response.data else { return }
        completion(dataImage)
      }
    }
}
