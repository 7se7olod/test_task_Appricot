import Foundation

enum StatusCharacter: String {
  case alive = "Alive"
  case dead = "Dead"
}


enum TitleController: String {
  case detail = "Detail information"
  case all = "All characters"
}
